import pandas as pd
from argparse import ArgumentParser
import json

import torch
from torch.utils.data import DataLoader

from transformers import AutoTokenizer

import pyarrow as pa
from datasets import Dataset
from tqdm.auto import tqdm

from src.util import custom_metrics


def parse_args():
    """
    This function defines and parses command-line arguments for the script.
    :return: Values of parsed arguments
    """
    parser = ArgumentParser()

    parser.add_argument("-tp", "--test_path",
                        type=str,
                        default="data/processed/test_data.csv",
                        required=False,
                        help="Path to test data"
                        )

    parser.add_argument("-mp", "--model_path",
                        type=str,
                        default="models/training/new_model_5.pt",
                        required=False,
                        help="Path to model being evaluated"
                        )

    parser.add_argument("-p", "--precise",
                        type=bool,
                        default=True,
                        required=False,
                        help="Precise or relaxed evaluation"
                        )

    return parser.parse_args()


def load_label_dict():
    """
    This function loads the saved label dictionary to properly map the labels to their corresponding ids.
    Additionally, it creates a reversed label dictionary to easily convert the predicted ids back into labels.
    :return: A label dictionary mapping labels to ids, together with a reversed label dictionary mapping ids to labels
    """
    with open("data/raw/mappings/label_dict.json", "r") as f:
        label_dict = json.load(f)

    reverse_label_dict = {v: k for k, v in label_dict.items()}
    return label_dict, reverse_label_dict


def main(test_path="data/processed/test_data.csv", model_path="models/training/new_model_5.pt", precise=True):
    """
    This function is the main function of the script. It preprocesses the test data and evaluates the trained model.
    It saves the predicted labels to a file called predicted_test_data.csv.
    Finally, it converts the pytorch model to a torchscript model with the average f1 score in the file name.
    :param test_path: The path to the test data
    :param model_path: The path to the model for evaluation
    :param precise: If True, uses exact matching for the scores. If False, it also marks predictions as correct when the
    actual label is at a shallower level in the taxonomy than the predicted label.
    :return:
    """
    # initialise device and tokenizer
    device = torch.device("cuda") if torch.cuda.is_available() else torch.device("cpu")
    tokenizer = AutoTokenizer.from_pretrained('malteos/scincl')

    # initialise train dataframe and remove unnecessary columns
    test_df = pd.read_csv(test_path)
    test_df = test_df[["title", "abstract", "label"]]
    test_df["abstract"] = ["" if pd.isna(abstract) else abstract for abstract in test_df["abstract"]]
    test_df["text"] = [row["title"] + tokenizer.sep_token + (row["abstract"] or "") for index, row in test_df.iterrows()]

    # define label dictionary by giving each label an id
    label_dict, reverse_label_dict = load_label_dict()
    test_df["label"] = test_df["label"].map(label_dict)

    test_df = test_df[["text", "label"]]

    # convert to HuggingFace dataset
    test_dataset = Dataset(pa.Table.from_pandas(test_df))

    # tokenize the text in the dataset
    def tokenize_function(examples):
        # tokenize the text
        tokenized_examples = tokenizer(
            examples["text"],
            padding="max_length",
            truncation=True,
            max_length=512,
        )

        # pad the attention masks to the same length as the input sequences
        tokenized_examples['attention_mask'] = [torch.cat([torch.tensor(mask), torch.zeros(512 - len(mask))]) for mask in tokenized_examples['attention_mask']]

        return tokenized_examples

    tokenized_test_dataset = test_dataset.map(tokenize_function, batched=True)

    # postprocessing
    # remove unnecessary columns from dataset
    tokenized_test_dataset = tokenized_test_dataset.remove_columns(["text"])

    # rename the label column to labels because the model expects the argument to be named as the latter
    tokenized_test_dataset = tokenized_test_dataset.rename_column("label", "labels")

    # set the format of the dataset to return PyTorch instead of lists
    tokenized_test_dataset.set_format("torch")

    # initialise the model
    model = torch.load(model_path)

    # test the model
    test_dataLoader = DataLoader(tokenized_test_dataset, batch_size=8)

    progress_bar = tqdm(range(len(test_dataLoader)))

    model.eval()
    predictions = []
    labels = []
    for batch in test_dataLoader:
        batch = {k: v.to(device) for k, v in batch.items()}
        with torch.no_grad():
            outputs = model(**batch)

        logits = outputs.logits
        predictions.extend(torch.argmax(logits, dim=1).tolist())
        labels.extend(batch["labels"].tolist())
        progress_bar.update(1)

    # visualise predicted labels as new column in test dataset
    predicted_labels = [reverse_label_dict[label] for label in predictions]
    final_df = pd.read_csv(test_path)
    final_df = final_df[["title", "abstract", "label"]]
    final_df["predicted_labels"] = predicted_labels
    final_df.to_csv("data/processed/predicted_test_data.csv", index=False)

    if precise:
        f1 = custom_metrics.get_score_classes()
    else:
        f1 = custom_metrics.get_relaxed_score()

    # convert to torchscript model
    for batch in test_dataLoader:
        batch = {k: v.to(device) for k, v in batch.items()}
        batch.pop("labels")
        traced_model = torch.jit.trace(model, example_kwarg_inputs=batch, strict=False)
        traced_model.save(f"models/traced_evaluated_model_{round(f1 * 1000)}.pt")
        break

    print('model evaluated!')


if __name__ == '__main__':
    args = parse_args()

    tp = args.test_path
    mp = args.model_path
    p = args.precise

    main(tp, mp, p)
